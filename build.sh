#!/bin/sh
echo "This script does no error checking after every step. Please watch the script's output including errors."
ppcrosswasm32 -Twasi -Cn tic80demo.pas
echo "Now edit ppas.sh to add --allow-undefined"
echo "Press enter to continue..."
read _x
./ppas.sh
tic80 --fs . --cmd 'load wasmdemo.wasmp & import binary tic80demo & run & exit'
echo "You should see ]]]]]]] on screen. This is incorrect behaviour. Press esc to bring up the console's menu, and quit the game."
echo "Now edit ppas.sh to add --allow-undefined and --stack-first"
echo "Press enter to continue..."
read _x
./ppas.sh
tic80 --fs . --cmd 'load wasmdemo.wasmp & import binary tic80demo & run & exit'
echo "You should see a sprite and some words on screen. Press arrow keys on the keyboard to move the sprite."
echo "You should also see the sprite disappearing about every other second.  This is incorrect behaviour. Is the Pascal Wasm program dropping output?"
