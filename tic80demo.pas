library tic80demo;

// TIC-80 API functions.
function btn(id: int32): int32; cdecl; external;
procedure cls(color: int8); cdecl; external;
function print(const txt: string; x, y: int32; color, fixed, scale, alt: int8): int32; cdecl; external;
procedure spr(id, x, y: int32; transcolors: puint8; colorCount: uint8; scale, flip, rotate, w, h: int32); cdecl; external;

var
  t, x, y: int8;

// Callback for TIC-80, run once.
procedure BOOT; cdecl;
begin
  t := 1;
  x := 96;
  y := 24;
end;

// Callback for TIC-80 game loop.
procedure TIC; cdecl;
begin
  if btn(0) > 0 then dec(y);
  if btn(1) > 0 then inc(y);
  if btn(2) > 0 then dec(x);
  if btn(3) > 0 then inc(x);

  cls(13);
  spr(1 + (((t mod 60) div 30) * 2), x, y, nil, 0, 3, 0, 0, 2, 2);
  print('HELLO WORLD, FROM PASCAL!', 48, 84, 15, 1, 1, 0);
  inc(t);
end;

exports
  BOOT name 'BOOT',
  TIC name 'TIC';

end.
